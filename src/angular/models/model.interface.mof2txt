[module model(MOF) /]
[import "../../util.mof2txt" /]
[import "../../util.angular.mof2txt" /]

@code-explicit
template public ext(c : Class)
  let sc: Class = c.generalization -> first()
    "extends " + helpers.jsClassName(sc.general.name)
  /let
/template

@code-explicit
template public importClass(c : NamedElement, mainClass : NamedElement, append: String = "")
  if(not tsUtil.isPrimitive(c))
    aUtil.importClass(c, mainClass)
  /if
/template

@code-explicit
template public generateInterface(concept: UMLDomainConcept)
  let classifier = concept.classifier
  if(classifier.oclIsKindOf(Classifier))
    let basename = config.client.path + "/" + config.client.apppath + "/models/" + classifier.name
      file( basename +".interface.ts")
        interfaceWrap(classifier)
      /file
    /let
  /if
  /let
/template

@code-explicit
template public generateInterfaces(d: DomainModel)
  generateInterface(d.domainElements)
/template

@code-explicit
template public interfaceWrap(c : Classifier) 
  if(c.generalization)
    importClass(c.generalization.general, c, ".model")

    importClass(
        c.ownedOperation.ownedElement->select(p | p.oclIsKindOf(Parameter))
        ->union(
            c.ownedElement->select(a | a.oclIsKindOf(Property))
        )
        ->collect(o | o.type)->asSet()
        ->select(eld | eld.name <> "Class" and eld.name <> c.name and c.generalization.general->forAll(sc | sc.name <> eld.name))
        , c, ".model")
  /if

  interfaceTs(c)
/template

@code-explicit
template public attributeLineTs(a: Property)
  a.name
  if(tsUtil.optional(a))
  "?"
  /if  
  ": " aUtil.getMongoType(a) tsUtil.addDefaultValue(a) "; // " a.type.name
/template

@text-explicit
[template public interfaceTs(c : Classifier) ]
// [protected(c.name + "imports")]

// [/protected]

export class [c.name] [ext(c)] {
  _id: string;
[for(a : Property | c.ownedElement) ?(not a.isDerived and a.redefinedProperty->isEmpty() and not sUtil.isCollection(a))]
  [attributeLineTs(a)]
[/for]

// [protected(c.name + "custom")]

// [/protected]
}
[/template]

