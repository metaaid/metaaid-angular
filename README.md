# Requirement

Metaaid `yarn add -g metaaid`
For running the Mongo-DB Server [Docker](https://docs.docker.com/get-docker/) is required.

IFML Editor - ifml.io
Uml Editor - Modelio

# Components

The angular project generates 3 main components: A database server connected to a Node-Js server for the CRUD-Backend and the actual Angular application for the frontend.

## Mongo-DB Server

The Mongo

## CRUD-Server

# Install

`yarn create metaaid-angular my-app`

`metaaid init -f angular-mongodb`

> yarn init ...

> metaaid: domai nmodel path: uml/domain.uml
> metaaid: ifml model path:  uml/interaction.ifml

this will result in the metaiaid.yaml and package.json

`metaaid install`

> yarn install

`metaaid generate`

`mof2txt -m tests/ressources/TodoList.xmi -t src/server/model.mof2txt src-gen/`
`mof2txt -m uml/todo.xmi -t src/server/model.mof2txt src-gen/`
`mof2txt -m uml/todo.ifml -t src/angular/main.mof2txt src-gen/`

# oncreate
in client:

`ng new todolist --skip-git --strict --routing --style sass --directory .`

# watch
`ng serve`

# install
yarn add -D tailwindcss postcss autoprefixer
yarn tailwindcss init
