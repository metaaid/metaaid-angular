- [x] add visual attribute "done"
- [x] fix todo.ifml
  - [x] uml_1:name ?
    - [x] no namespace required for attributes
- [x] visualize "done"
- [ ] minimal to use
  - [ ] Nested (levels)
  - [ ] direct edit
  - [ ] single page
  - [ ] Labels (Today f.e.)
- [x] OrderBy
- [ ] error message on no api
- [ ] success messages
- [ ] Better names for
  - [ ] Navigation
  - [ ] Form fields
ifml io fix:
- [ ] save outInteractionFlows on events

Might need fix:
  - [ ] elements (IFMLConnection still have id's as id)
  - [ ] parameters not stored in paramaeters anymore -> viewcomponents
  - [ ] auto parameter (direction "in") has no name for action
  - [ ] action parameter does not get deleted (only di?)
  - [ ] classifier still use a name instead of xmi:id
  - [ ] exsiting action parameter are not registered to id-registry -> results in error on use for auto-binding

Todolist
- [x] save entry to backend
  - [x] connect formgroup to parameter binding
  - [x] connect fields to attributes
- [ ] navigate from form to Todolist (Navigationflow on "saved")
  - [ ] XOR Navigation
- [x] list all entries
  - [x] List Component
  - [ ] General Component
  - [ ] Events on List Entries ?
    - [x] OnSelectEvents
      - [ ] Show not Show Detail/ViewComponent
    - [ ] Add Delete Event
      - [ ] Parameterbinding from Databinding
      - [ ] custom delete action
    - [ ] Add Edit Event
  - [ ] Reload List ?
- [ ] navigation concept
  - [ ] impressum (L)
  - [ ] XOR
- [ ] Create DetailView
  - [ ] Parameter passing
- [ ] Create DetailView Modal

Editor Fixes:
- [ ] XMI-ID instead of name on classifier
- [ ] "id" -> "xmi:id" on some nodes

Mof2Txt fixes:
- [ ] Error stack keeps stacking with each error and is not resetted

Framework
- [ ] Use yarn namespaces
  - [ ] dev
    - [ ] database start
    - [ ] dev api backend (metaaid)
    - [ ] dev angular (metaaid)

Research
- [ ] Angular Forms
  - [ ] reactive/template based
  - [ ] form groups/arrays
  - [ ] submit event
  - [ ] update data model
    - [ ] patchValue/setValue
  - [ ] submit -> save to database
- [ ] Angular Navigation

Install
  - [ ] use name for install


Editor (to work with current ifml-file):
  - [ ] Parameter Type for generated "in"-Paramter (in == out)
  - [ ] Save Formfields as parameters on Form (not onSubmitEvent)
  - [ ] Vievcomponent List
  - [ ] fix domainElements
    - [ ] no primitive types
    - [ ] real link to uml
    - [ ] fix visual representation "undefined"
    - [ ] DomainBinding.domainConcept is missing
  - [ ] add (outInteractionFlows/inInteractionFlows) to xmi working one of:
    - [ ] <outInteractionFlows>Flow_0t7m4aj</outInteractionFlows>
    - [ ] <outInteractionFlows xmi:idref="Flow_0t7m4aj" />
    - [ ] actionevents/onsubmitevents
