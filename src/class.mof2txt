[module tsClass(MOF)/]
[import "util.mof2txt" /]

[query public dependencyclassesWithSC(c: Class) : Set(Class) = c.ownedElement->select(a : Property |(not tsUtil.isPrimitive(a.type)))->union(c.generalization)->asSet()/]
[query public dependencyclasses(c: Class) : Set(Class) = c.ownedElement->select(a : Property | (not tsUtil.isPrimitive(a.type)))->asSet()/]
[query public isOperationWrapped(a: Property) : Boolean = (a.isDerived) /]
[query public isOperationWrapped2(a: Property) : Boolean = (a.isDerived or not a.redefinedProperty->isEmpty() or not a.subsettedProperty->isEmpty()) /]
[query public overwrittenByUser(e: Element) : Boolean = (helpers.isOverWrittenByUser(e)) /]

[template public importClass(c : NamedElement, mainClass : NamedElement)]
const c.name = require("[helpers.calcRelativePath(c, mainClass)]");
[/template]

[template public exportElement(c : PackageableElement, mainClass : PackageableElement)]
[if(c.oclIsKindOf("Package"))]
export { [helpers.jsClassName(c.name)] } from "[helpers.calcRelativePath(c, mainClass)]";
[else]
export { [helpers.jsClassName(c.name)] } from "[helpers.calcRelativePath(c, mainClass)]";
[/if]
[/template]

[template public exportPackage(c : PackageableElement, mainClass : PackageableElement)]

export * from "[helpers.calcRelativePath(c, mainClass)]/";
[/template]

@code-explicit
template public ext(c : Class)
if(tsUtil.useMultiInheritance(c))
"extends multiExtends(" + c.generalization.general.name + ")"
else
let sc : Class = c.generalization->first()
"extends " + helpers.jsClassName(sc.general.name)
else
helpers.defaultExtendsExpression(c)
/let
/if
/template

@text-explicit
[template public parameterTypeString(p: Object) ]
[tsUtil.typeToString(p.type, tsUtil.isCollection(p))]
[/template]

@text-explicit
[template public parameterString(p : Parameter) ]
[helpers.jsVarNames(p.name)]: [parameterTypeString(p)]
[/template]

@code-explicit
template public attributeName(a : Property)
if(isOperationWrapped(a)) "_" /if helpers.jsVarNames(a.name) if(not a.redefinedProperty->isEmpty()) "R" /if
/template

@text-explicit
[template public generate(p : Package) ]
[file (tsUtil.getPath(p) +'/index.ts', false)]

export function getName() {
    return "[helpers.jsClassName(p.name)]";
}
[let packageMerges = p.packageMerge]
[for(pEl | p.packagedElement)]
[if(not packageMerges.mergedPackage.ownedElement->exists(e | e.name = pEl.name))]
[exportElement(pEl, p)/]
[else]
//TODO: get [pEl.name] and merge it later
[/if]
[/for]
[/let]

// package imports ([p.packageImport->size()])

[for(pi | p.packageImport)]
[exportPackage(pi.importedPackage, p)/]
[/for]

// package merges ([p.packageMerge->size()])
[for(pm | p.packageMerge)]
export { [helpers.jsClassName(pm.mergedPackage.ownedElement.name)] } from "[helpers.calcRelativePath(pm.mergedPackage, p)]/";
[/for]

// create Defaultpackage
import { Package } from "[helpers.calcRelativePath(null, p)]/UML/";

[importClass(p.packagedElement->select(el | el.name <> "Package")->asSet(), p)/]
export class [helpers.jsClassName(p.name)] { //  extends Package
[for(sub | p.packagedElement)]
    /*
    [sub.name/] = [sub.name/](); // new
    */
[/for]

    buildMetaClass(){
        return [p.name].buildStaticMetaClass();
    }

    static buildStaticMetaClass() {
        let UML = require('[helpers.calcRelativePath(null, p)]/UML/');
        //let tsClass = require('[helpers.calcRelativePath(p.metaclass, p)]').[p.metaclass.name];
        let tsClass = UML.Package;
        let metaClass = new tsClass();

        metaClass.name = "[p.name]";
        [let owner = p.owner->first()]
        [if(owner.name)]
        metaClass.ownerAddSubSet(UML.[owner.name]);
        [/if]
        [/let]
        return metaClass;
    }
}
[/file]
# outside!
[generate(p.ownedElement)]
[generateClass(p.ownedElement)]
[/template]

@text-explicit
[template public generateClass(c : Classifier) ]
[file (tsUtil.getPath(c) +'.ts', false)]
// PATH: [tsUtil.getPath(c)]
[if(c.generalization)]
[importClass(c.generalization.general, c)/]
[/if]
[if (c.generalization->isEmpty() and c.name <> "Object")]
import { Element as MOFElement } from '[helpers.calcRelativePath("MOF/Reflection/Element", c, false)]';
[/if]

import { A_subsettedProperty_property as SubProp } from "[helpers.calcRelativePath("/../util/MetaFunctions", c, false)]";
[if(tsUtil.useMultiInheritance(c))]
import { multiExtends } from "[helpers.calcRelativePath("/../util/MetaFunctions", c, false)]";
[/if]

export class [helpers.jsClassName(c.name)] [ext(c)]{
    [for(a : Property | c.ownedElement)]
    // [a.name] der:[a.isDerived] redefined:[not a.redefinedProperty->isEmpty()] [a.redefinedProperty->size()] collection:[tsUtil.isCollection(a)] derUnion: [a.isDerivedUnion] subsetted:[not a.subsettedProperty->isEmpty()] [a.subsettedProperty->size()] composite:[a.isComposite()] hasOperation: [helpers.hasOperation(a.name, c)] isOverwritten: [overwrittenByUser(a)]
    [/for]

    constructor() {
        [if(c.name <> 'Object')]super();[/if]
    [for(a : Property | c.ownedElement) ? ((not a.isDerivedUnion) and (not a.subsettedProperty->isEmpty()) and a.isDerived)]
    [for(s : Property | a.subsettedProperty)]
        [if(s.owner.oclIsKindOf(Class) and not helpers.hasOperation(a.name, c))]
        [if(s.isDerived)]
        this._[s.name].AddSubSet(this.[a.name]);
        [else]
        this.[s.name].AddSubSet(this.[a.name]);
        [/if]
        [else]
        // TODO: maybe make all arrays subsetable, this should be also a subset: [s.name] > [a.name]
        [/if]
    [/for]
    [/for]
    }

    [for(a : Property | c.ownedElement) ? (not overwrittenByUser(a) and not a.isDerived)]
/** [a.ownedComment.body]
     */
    [attributeName(a)]: [tsUtil.typeToString(a.type, tsUtil.isCollection(a), not a.subsettedProperty->isEmpty())][tsUtil.addDefaultValue(a)]; //subsettedProperty [not a.subsettedProperty->isEmpty()]
    [if(tsUtil.isCollection(a))]
    get [attributeName(a)]All(): Array<[tsUtil.typeToString(a.type)]> {
        return this.[attributeName(a)] instanceof Array ? this.[attributeName(a)] : \[this.[attributeName(a)]];
    }
    [/if]
    [/for]

    // isDerivedUnion
    [for(a : Property | c.ownedElement) ? (not overwrittenByUser(a) and a.isDerivedUnion and a.isDerived and a.subsettedProperty->isEmpty() and not helpers.hasOperation(a, c))]
    /** [a.ownedComment.body]
     */
    _[a.name/]: SubProp<[tsUtil.typeToString(a.type)]> = SubProp.create<[tsUtil.typeToString(a.type)]>();

    get [a.name](): SubProp<[tsUtil.typeToString(a.type)]> {
        [if(tsUtil.isCollection(a))]
        // this._[a.name].filter(...)
        return this._[a.name];
        [else]
        return this._[a.name];
        [/if]
    }
    [a.name]AddSubSet(subset: [tsUtil.typeToString(a.type)]) {
        // TODO: split by ... Type !?
        this._[a.name].push(subset);
    }
    [/for]

    /* subsettedProperties
    [for(a : Property | c.ownedElement) ? ((not a.isDerivedUnion) and (not a.subsettedProperty->isEmpty()) and a.isDerived)]
    [if(not helpers.hasOperation(a.name, c))]
    [let first = a.subsettedProperty->first()]
    /** [a.ownedComment.body]
     */
    get [a.name](): [tsUtil.typeToString(a.type, tsUtil.isCollection(a))] {
        [if (tsUtil.isCollection(a))]
        return this.[first.name].filter(e => e instanceof [tsUtil.typeToString(a.type)]);
        [else]
        return this.[first.name] as [tsUtil.typeToString(a.type)];
        [/if]
    }
    set [a.name](val: [tsUtil.typeToString(a.type, tsUtil.isCollection(a))]) {
        [if(not a.isReadOnly)]
        if(typeof this.[first.name] != "undefined"){
            console.warning("readonly attribute [first.name] was overwritten");
        }
        [/if]
        [let first = a.subsettedProperty->first()]
        this.[first.name] = val;
        [/let]
    }
    [if (tsUtil.isCollection(a))]
    add[a.name->toUpperFirst()](val: [tsUtil.typeToString(a.type)]): void{
        this.[first.name].push(val);
    }
    [/if]
    [/let]
    [/if]
    [/for]*/

    //operations
    [for(o : Operation | c.ownedOperation) ? (not overwrittenByUser(o))]
    [let ret = o.ownedElement->select(p | p.oclIsKindOf(Parameter) and p.direction = "return")]
    [let parameters = o.ownedElement->select(p | p.oclIsKindOf(Parameter) and p.direction <> "return")]
    /** [o.ownedComment.body]
     */
    [if(helpers.hasAttribute(o.name, c))]get [/if][o.name]( [parameters->collect(p | parameterString(p))] ): [if(ret->isEmpty() or ret.type->oclIsUndefined())]void[else][parameterTypeString(ret)][/if] { //: [tsUtil.typeToString(ret.type)][if (tsUtil.isCollection(ret))]\[][/if]
        [if(o.bodyCondition)]
        let oclString = `[helpers.parseOCL(o.bodyCondition.specification.body)]`;
        return this.executeOCL(oclString);
        [else]
// [protected (c.name + ":" + o.name)]
// [/protected]
        [/if]
    }
    [/let]
    [/let]
    [/for]

    buildMetaClass(): Class{
        return [helpers.jsClassName(c.name)].buildStaticMetaClass();
    }

    static buildStaticMetaClass() : Class {
        let UML = require('[helpers.calcRelativePath(null, c)]/UML/');
        let tsClass = UML.[c.metaclass.name];
        let metaClass = new tsClass();
        [if(not c.generalization.isEmpty())]
        let generalization;
        [for (g | c.generalization)]
        generalization = new UML.Generalization();
        generalization.general = require('[helpers.calcRelativePath(g.general, c)]').[g.general.name]
        metaClass.generalization.push(generalization);
        [/for]
        [/if]
        metaClass.name = "[c.name]";
        // metaClass.ownerAddSubSet(UML.[c.owner.name]);
        let ownerClass = require("[helpers.calcRelativePath(c.owner->first(), c)]").[c.owner.name]
        metaClass.ownerAddSubSet(ownerClass);
        [let attr = c.ownedElement]
        let propertyClass;
        let tmpProperty;

        [for(a : Property | attr)]
        propertyClass = UML.[a.metaclass.name];
        tmpProperty = new propertyClass();
        tmpProperty.name = "[a.name]";
        tmpProperty.type = require("[helpers.calcRelativePath(a.type, c, false)]").[a.type.name];

        //TODO: multiplicity
        [if (a.defaultValue)]
        tmpProperty.default = [helpers.parseDefaultValue(a.defaultValue.value, a)];
        [/if]
        metaClass.set(tmpProperty);
        [/for]
        [/let]
        return metaClass;
    }

    // user operations
    // [protected (c.name)]
    // [/protected]
}
[if(c.name <> 'Class' and c.generalization.general->forAll(sc | sc.name <> "Class"))]
import { Class } from '[helpers.calcRelativePath(null, c)]/UML/';
[/if]
// # of imports [dependencyclasses(c)->size()]

[if(tsUtil.useMultiInheritance(c))]
export interface [helpers.jsClassName(c.name)] extends [helpers.jsClassName(c.generalization.general.name)]{};
[/if]

// imports from attributes and operations
[importClass(
    c.ownedOperation.ownedElement->select(p | p.oclIsKindOf(Parameter))
    ->union(
        c.ownedElement->select(a | a.oclIsKindOf(Property))
    )
    ->collect(o | o.type)->asSet()
    ->select(eld | eld.name <> "Class" and eld.name <> c.name and c.generalization.general->forAll(sc | sc.name <> eld.name))
    , c)]

// user imports
// [protected (c.name + "-imports")]
// [/protected]
[/file]
[/template]
